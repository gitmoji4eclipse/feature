# Gitmoji 4 Eclipse - Feature

This project provides the feature project of the **gitmoji4eclipse** plug-in.

The plug-in project is hosted [here](https://gitlab.com/gitmoji4eclipse/plugin), 
and the update-site project [here](https://gitlab.com/gitmoji4eclipse/update-site).

Get more information and an how-to on the 
[update-site page](https://gitmoji4eclipse.gitlab.io/update-site).
